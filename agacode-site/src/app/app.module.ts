import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeModule } from './pages/home/home.module';
import { ApiService } from './services/api.service';
import { HttpClientModule } from '@angular/common/http';
import { ToastComponent } from './components/toast/toast.component';
import { ToastService } from './services/toast.service';


@NgModule({
  declarations: [
    AppComponent,
    ToastComponent
  ],
  imports: [
    BrowserModule,
    HomeModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [ApiService, ToastService],
  bootstrap: [AppComponent]
})
export class AppModule { }
